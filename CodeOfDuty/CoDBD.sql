﻿-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2017 at 09:29 PM
-- Server version: 5.7.20-ndb-7.5.8-cluster-gpl
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CoD`
--

-- --------------------------------------------------------

--
-- Table structure for table `Answers`
--

CREATE TABLE `Answers` (
  `AnswerID` int(11) DEFAULT NULL,
  `Answer` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Answers`
--

INSERT INTO `Answers` (`AnswerID`, `Answer`) VALUES
(1, 'Hello, World'),
(2, '120');

-- --------------------------------------------------------

--
-- Table structure for table `Codes`
--

CREATE TABLE `Codes` (
  `CodeID` int(11) DEFAULT NULL,
  `Code` varchar(3000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Codes`
--

INSERT INTO `Codes` (`CodeID`, `Code`) VALUES
(1, 'public class HelloWorld {\n\n            public static void main(String[] args) {\n                System.out.println(\"Hello, World\");\n            }\n\n        }'),
(2, 'public class bdfactoriel {\n\n    public static void main(String[] args) {\n        System.out.println(factorial());\n    }\n\n    private static long factorial() {\n        int fact;\n        fact = (5 * 4 * 3 * 2 * 1);\n        return fact;\n    }\n}');

-- --------------------------------------------------------

--
-- Table structure for table `Questions`
--

CREATE TABLE `Questions` (
  `QuestionID` int(11) DEFAULT NULL,
  `Question` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Questions`
--

INSERT INTO `Questions` (`QuestionID`, `Question`) VALUES
(1, 'Veuillez ecrire Hello, World a la console'),
(2, 'Veuillez faire une classe qui calcule le factoriel de 5');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
