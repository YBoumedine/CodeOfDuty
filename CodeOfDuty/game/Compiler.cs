﻿using System;
using System.Diagnostics;
using Microsoft.Win32;
using System.IO;
using System.Text;

namespace CodeOfDuty
{
    class Compiler
    {
        public static void transfromTextToFile(String[] text)
        {
            string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\test.java"))
            {
                foreach (string line in text)
                    outputFile.WriteLine(line);
            }

        }

        public static String compileProgram()
        {
            string installPath = GetJavaInstallationPath();
            string jfilePath = System.IO.Path.Combine(installPath, "bin\\javac.exe");


            // We have a winner
            string docpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string filepath = (docpath + "/test.java");
            System.Diagnostics.Process compilerProcess = new System.Diagnostics.Process();
            compilerProcess.StartInfo.FileName = jfilePath;
            compilerProcess.StartInfo.Arguments = filepath;
            compilerProcess.StartInfo.UseShellExecute = false;
            compilerProcess.StartInfo.RedirectStandardOutput = true;
            compilerProcess.StartInfo.RedirectStandardError = true;
            compilerProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            compilerProcess.StartInfo.CreateNoWindow = true; //not diplay a windows
            compilerProcess.Start();
            string output = compilerProcess.StandardError.ReadToEnd(); //The output result
            if (compilerProcess.ExitCode == 0) return "no err";
            return output;
        }

        public static String runProgram()
        {
            string installPath = GetJavaInstallationPath();
            string jfilePath = System.IO.Path.Combine(installPath, "bin\\java.exe");
            string docpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string filepath = (docpath + "@test.java");
            System.Diagnostics.Process runProcess = new System.Diagnostics.Process();
            runProcess.StartInfo.FileName = jfilePath;
            runProcess.StartInfo.Arguments = "-cp " + docpath + " test";
            runProcess.StartInfo.UseShellExecute = false;
            runProcess.StartInfo.RedirectStandardOutput = true;
            runProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            runProcess.StartInfo.CreateNoWindow = true; //not diplay a windows
            runProcess.Start();
            string output = runProcess.StandardOutput.ReadToEnd();

            Console.WriteLine("Inside runProgram : " + output);

            return output;
        }

        public static string GetJavaInstallationPath()
        {
            string environmentPath = Environment.GetEnvironmentVariable("JAVA_HOME");
            if (!string.IsNullOrEmpty(environmentPath))
            {
                return environmentPath;
            }

            const string JAVA_KEY = "SOFTWARE\\JavaSoft\\Java Development Kit\\";

            var localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);
            using (var rk = localKey.OpenSubKey(JAVA_KEY))
            {
                if (rk != null)
                {
                    string currentVersion = rk.GetValue("CurrentVersion").ToString();
                    using (var key = rk.OpenSubKey(currentVersion))
                    {
                        return key.GetValue("JavaHome").ToString();
                    }
                }
            }

            localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            using (var rk = localKey.OpenSubKey(JAVA_KEY))
            {
                if (rk != null)
                {
                    string currentVersion = rk.GetValue("CurrentVersion").ToString();
                    using (var key = rk.OpenSubKey(currentVersion))
                    {
                        return key.GetValue("JavaHome").ToString();
                    }
                }
            }
            return null;
        }
    }
}
