using System;
using System.Data.SqlClient;



namespace CodeOfDuty
{
    class DataBaseConnection
    {
        private static string question;
        private static string answer;
        private static string code;
        private static int questionID;

        public static String GetQuestion(int id)
        {
            try
            {
                //Open a connection to the SQL server
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                conn.Open();

                //Find the question based on the id in parameter
                SqlCommand cmd = new SqlCommand("SELECT Question FROM QUESTIONS WHERE QuestionID =" + id, conn);
                question = cmd.ExecuteScalar().ToString();
                conn.Close();
                return question; 

            }
            catch (SqlException oError)
            {
                return oError.ToString();
            }
            

           
        }

        public static int GetQuestionId(int id)
        {
            try
            {
                //Open a connection to the SQL server
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                conn.Open();

                //Find the question based on the id in parameter
                SqlCommand cmd = new SqlCommand("SELECT QuestionID FROM QUESTIONS WHERE QuestionID =" + id, conn);
                questionID = (int) cmd.ExecuteScalar();

                conn.Close();
                return questionID;

            }
            catch (SqlException oError)
            {
                Console.WriteLine(oError);
                return 0;
            }
        }

        public static String getAnswer(int id)
        {
            try
            {
                //Open a connection to the SQL server
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                conn.Open();

                //Find the question based on the id in parameter
                SqlCommand cmd = new SqlCommand("SELECT Answer FROM Answers WHERE AnswerID="+id, conn);
                answer = cmd.ExecuteScalar().ToString();


                conn.Close();

                return answer;

            }
            catch (SqlException oError)
            {
                return oError.ToString();
            }
        }

        public static String getCode()
        {
            try
            {
                //Open a connection to the SQL server
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                conn.Open();

                //Find the question based on the id in parameter
                SqlCommand cmd = new SqlCommand("SELECT Code FROM Codes WHERE CodeID=1", conn);
                code = cmd.ExecuteScalar().ToString();
                conn.Close();
                return code;


            }
            catch (SqlException oError)
            {
                return oError.ToString();
            }
        }
    }
}