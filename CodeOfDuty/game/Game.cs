using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.CSharp;
using System.Threading.Tasks;

namespace CodeOfDuty.game
{
    class Game
    {
        public static Dictionary<Int32, Question> questionsDictionary = new Dictionary<Int32, Question>();

        public static void CreateQuestionDictionnary()
        {

            for (int i = 1; i < 5; i++)
            {
                questionsDictionary.Add(i, (new Question(i, DataBaseConnection.GetQuestion(i), DataBaseConnection.getCode(), DataBaseConnection.getAnswer(i))));
            }
            Console.WriteLine(questionsDictionary.Count);
        }

        public static void loadDictionnary()
        {
            int a = questionsDictionary.Count - 1;
            while (a >= 0)
            {
                questionsDictionary.ElementAt(a).Value.id = a+1;
                questionsDictionary.ElementAt(a).Value.question = (DataBaseConnection.GetQuestion(a+1));
                questionsDictionary.ElementAt(a).Value.code = (DataBaseConnection.getCode());
                questionsDictionary.ElementAt(a).Value.answer = (DataBaseConnection.getAnswer(a+1));
                a--;
            }
        }

        private static String callCompile(String programCode)
        {
            String[] codeArray = { programCode };
            Compiler.transfromTextToFile(codeArray);
            string output = Compiler.compileProgram();
            return output;
        }

        private static String callRun(int i)
        {
            string result = Compiler.runProgram();
            string dBAnswer = DataBaseConnection.getAnswer(i);
            string returnValue;

            Console.WriteLine("program result : " + result + " db result: " + dBAnswer);

            if (String.Equals(result, dBAnswer))
            {
                returnValue = "Bonne reponse";
            }
            else
            {
                returnValue = "Mauvaise reponse";
            }

            return returnValue;
        }


        public static String checkAnswer(String programCode, int i)
        {
            string resultCompile = callCompile(programCode);
            if (String.Equals(resultCompile, "no err"))
            {
                return callRun(i);
            }
            else
            {
                return resultCompile;
            }
        }
    }
}
