using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace CodeOfDuty
{
    class Question
    {
        public string question { get; set; }
        public string answer { get; set; }
        public string code { get; set; }
        public int id { get; set; }

        public Question() { }
        
        public Question(int identificateur, String q, String cod, String answ)


        {
            question = q;
            id = identificateur;
            answer = answ;
            code = cod;
        }


        public String getQuestion()
        {
            return question;
        }

        
    }
}
