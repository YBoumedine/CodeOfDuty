﻿namespace CodeOfDuty
{
    partial class GameChoiceInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.multiplayerButton = new System.Windows.Forms.Button();
            this.tutorialButton = new System.Windows.Forms.Button();
            this.backChoiceButton = new System.Windows.Forms.Button();
            this.singleplayerButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // multiplayerButton
            // 
            this.multiplayerButton.BackColor = System.Drawing.Color.Black;
            this.multiplayerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiplayerButton.ForeColor = System.Drawing.SystemColors.Control;
            this.multiplayerButton.Location = new System.Drawing.Point(255, 167);
            this.multiplayerButton.Name = "multiplayerButton";
            this.multiplayerButton.Size = new System.Drawing.Size(199, 76);
            this.multiplayerButton.TabIndex = 0;
            this.multiplayerButton.Text = "Multiplayer";
            this.multiplayerButton.UseVisualStyleBackColor = false;
            this.multiplayerButton.Click += new System.EventHandler(this.multiplayerButton_Click);
            // 
            // tutorialButton
            // 
            this.tutorialButton.BackColor = System.Drawing.Color.Black;
            this.tutorialButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tutorialButton.ForeColor = System.Drawing.SystemColors.Control;
            this.tutorialButton.Location = new System.Drawing.Point(255, 274);
            this.tutorialButton.Name = "tutorialButton";
            this.tutorialButton.Size = new System.Drawing.Size(199, 76);
            this.tutorialButton.TabIndex = 1;
            this.tutorialButton.Text = "Tutorial";
            this.tutorialButton.UseVisualStyleBackColor = false;
            this.tutorialButton.Click += new System.EventHandler(this.tutorialButton_Click);
            // 
            // backChoiceButton
            // 
            this.backChoiceButton.BackColor = System.Drawing.Color.Black;
            this.backChoiceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backChoiceButton.ForeColor = System.Drawing.SystemColors.Control;
            this.backChoiceButton.Location = new System.Drawing.Point(255, 386);
            this.backChoiceButton.Name = "backChoiceButton";
            this.backChoiceButton.Size = new System.Drawing.Size(199, 76);
            this.backChoiceButton.TabIndex = 2;
            this.backChoiceButton.Text = "Back";
            this.backChoiceButton.UseVisualStyleBackColor = false;
            this.backChoiceButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // singleplayerButton
            // 
            this.singleplayerButton.BackColor = System.Drawing.Color.Black;
            this.singleplayerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.singleplayerButton.ForeColor = System.Drawing.SystemColors.Control;
            this.singleplayerButton.Location = new System.Drawing.Point(255, 57);
            this.singleplayerButton.Name = "singleplayerButton";
            this.singleplayerButton.Size = new System.Drawing.Size(199, 76);
            this.singleplayerButton.TabIndex = 3;
            this.singleplayerButton.Text = "Singleplayer";
            this.singleplayerButton.UseVisualStyleBackColor = false;
            this.singleplayerButton.Click += new System.EventHandler(this.singleplayerButton_Click);
            // 
            // GameChoiceInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(743, 588);
            this.Controls.Add(this.singleplayerButton);
            this.Controls.Add(this.backChoiceButton);
            this.Controls.Add(this.tutorialButton);
            this.Controls.Add(this.multiplayerButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GameChoiceInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "tex";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.GameChoiceInterface_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button multiplayerButton;
        private System.Windows.Forms.Button tutorialButton;
        private System.Windows.Forms.Button backChoiceButton;
        private System.Windows.Forms.Button singleplayerButton;
       
    }
}