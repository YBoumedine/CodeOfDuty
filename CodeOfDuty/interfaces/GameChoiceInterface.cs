﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeOfDuty
{
    public partial class GameChoiceInterface : Form
    {
        public GameChoiceInterface()
        {
            InitializeComponent();
        }

        private void GameChoiceInterface_Load(object sender, EventArgs e)
        {

        }

        private void multiplayerButton_Click(object sender, EventArgs e)
        {
            MultiplayerWait mult = new MultiplayerWait();
            this.Hide();
            mult.ShowDialog();
            this.Show();
        }

        private void tutorialButton_Click(object sender, EventArgs e)
        {
            TutorialInterface game = new TutorialInterface();
            this.Hide();
            game.ShowDialog();
            this.Show();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void singleplayerButton_Click(object sender, EventArgs e)
        {
            SingleplayerWait sing = new SingleplayerWait();
            this.Hide();
            sing.ShowDialog();
            this.Show();
        }
    }
}
