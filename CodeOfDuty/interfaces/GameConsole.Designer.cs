﻿using ScintillaNET;
using System;
using System.Drawing;

namespace CodeOfDuty
{
    partial class GameConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.presentation = new System.Windows.Forms.Label();
            this.compileButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.scintilla1 = new ScintillaNET.Scintilla();
            this.questionDescription = new System.Windows.Forms.Label();
            this.comparaisonReponseLabel = new System.Windows.Forms.Label();
            this.reponseCompilateurLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // presentation
            // 
            this.presentation.AutoSize = true;
            this.presentation.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.presentation.Location = new System.Drawing.Point(40, 62);
            this.presentation.Name = "presentation";
            this.presentation.Size = new System.Drawing.Size(330, 33);
            this.presentation.TabIndex = 1;
            this.presentation.Text = "Here is your question :";
            // 
            // compileButton
            // 
            this.compileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.compileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compileButton.Location = new System.Drawing.Point(851, 607);
            this.compileButton.Name = "compileButton";
            this.compileButton.Size = new System.Drawing.Size(348, 71);
            this.compileButton.TabIndex = 2;
            this.compileButton.Text = "Compile and test";
            this.compileButton.UseVisualStyleBackColor = true;
            this.compileButton.Click += new System.EventHandler(this.CompileButton_Click);
            // 
            // backButton
            // 
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(34, 674);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(348, 71);
            this.backButton.TabIndex = 3;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // scintilla1
            // 
            this.scintilla1.IndentWidth = 4;
            this.scintilla1.Lexer = ScintillaNET.Lexer.Cpp;
            this.scintilla1.Location = new System.Drawing.Point(639, 62);
            this.scintilla1.Name = "scintilla1";
            this.scintilla1.Size = new System.Drawing.Size(702, 539);
            this.scintilla1.TabIndex = 4;
            this.scintilla1.StyleResetDefault();
            this.scintilla1.Styles[Style.Default].Font = "Consolas";
            this.scintilla1.Styles[Style.Default].Size = 10;
            this.scintilla1.StyleClearAll();

            // Configure the CPP (C#) lexer styles
            this.scintilla1.Styles[Style.Cpp.Default].ForeColor = Color.Silver;
            this.scintilla1.Styles[Style.Cpp.Comment].ForeColor = Color.FromArgb(0, 128, 0); // Green
            this.scintilla1.Styles[Style.Cpp.CommentLine].ForeColor = Color.FromArgb(0, 128, 0); // Green
            this.scintilla1.Styles[Style.Cpp.CommentLineDoc].ForeColor = Color.FromArgb(128, 128, 128); // Gray
            this.scintilla1.Styles[Style.Cpp.Number].ForeColor = Color.Olive;
            this.scintilla1.Styles[Style.Cpp.Word].ForeColor = Color.Blue;
            this.scintilla1.Styles[Style.Cpp.Word2].ForeColor = Color.Blue;
            this.scintilla1.Styles[Style.Cpp.String].ForeColor = Color.FromArgb(163, 21, 21); // Red
            this.scintilla1.Styles[Style.Cpp.Character].ForeColor = Color.FromArgb(163, 21, 21); // Red
            this.scintilla1.Styles[Style.Cpp.Verbatim].ForeColor = Color.FromArgb(163, 21, 21); // Red
            this.scintilla1.Styles[Style.Cpp.StringEol].BackColor = Color.Pink;
            this.scintilla1.Styles[Style.Cpp.Operator].ForeColor = Color.Purple;
            this.scintilla1.Styles[Style.Cpp.Preprocessor].ForeColor = Color.Maroon;
            this.scintilla1.Lexer = Lexer.Cpp;

            // Set the keywords
            this.scintilla1.SetKeywords(0, "abstract as base break case catch checked continue default delegate do else event explicit extern false finally fixed for foreach goto if implicit in interface internal is lock namespace new null object operator out override params private protected public readonly ref return sealed sizeof stackalloc switch this throw true try typeof unchecked unsafe using virtual while");
            this.scintilla1.SetKeywords(1, "bool byte char class const decimal double enum float int long sbyte short static string struct uint ulong ushort void");
            // 
            // questionDescription
            // 
            this.questionDescription.AutoSize = true;
            this.questionDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.questionDescription.Location = new System.Drawing.Point(40, 280);
            this.questionDescription.Name = "questionDescription";
            this.questionDescription.Size = new System.Drawing.Size(623, 33);
            this.questionDescription.TabIndex = 5;
            // 
            // comparaisonReponseLabel
            // 
            this.comparaisonReponseLabel.AutoSize = true;
            this.comparaisonReponseLabel.Location = new System.Drawing.Point(117, 554);
            this.comparaisonReponseLabel.Name = "comparaisonReponseLabel";
            this.comparaisonReponseLabel.Size = new System.Drawing.Size(0, 13);
            this.comparaisonReponseLabel.TabIndex = 6;
            // 
            // reponseCompilateurLabel
            // 
            this.reponseCompilateurLabel.AutoSize = true;
            this.reponseCompilateurLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reponseCompilateurLabel.Location = new System.Drawing.Point(117, 404);
            this.reponseCompilateurLabel.Name = "reponseCompilateurLabel";
            this.reponseCompilateurLabel.Size = new System.Drawing.Size(0, 39);
            this.reponseCompilateurLabel.TabIndex = 7;
            // 
            // GameConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.reponseCompilateurLabel);
            this.Controls.Add(this.comparaisonReponseLabel);
            this.Controls.Add(this.questionDescription);
            this.Controls.Add(this.scintilla1);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.compileButton);
            this.Controls.Add(this.presentation);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GameConsole";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GameInterface";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label presentation;
        private System.Windows.Forms.Button compileButton;
        private System.Windows.Forms.Button backButton;
        private ScintillaNET.Scintilla scintilla1;
        private System.Windows.Forms.Label questionDescription;
        private System.Windows.Forms.Label comparaisonReponseLabel;
        private System.Windows.Forms.Label reponseCompilateurLabel;
    }

}