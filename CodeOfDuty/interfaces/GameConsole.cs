﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeOfDuty
{
    public partial class GameConsole : Form
    {
        public int questionID;

        public GameConsole()
        {
            InitializeComponent();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CompileButton_Click(object sender, EventArgs e)
        {
            String firstString = scintilla1.GetTextRange(0, Math.Min(500, scintilla1.TextLength));
            this.comparaisonReponseLabel.Text = game.Game.checkAnswer(firstString, questionID);
        }

        public void setQuestionDescription(string question)
        {
            this.questionDescription.Text = question;
        }
    }
}
