using System;
using Microsoft.CSharp;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace CodeOfDuty
{  
    public partial class MainInterface : Form
    {
        
        

        public MainInterface()
        {
            InitializeComponent();

        }

        private void MainInterface_Load(object sender, EventArgs e)
        {
            
        }

        private void startButtonClick(object sender, EventArgs e)
        {
            GameChoiceInterface choice = new GameChoiceInterface();
            this.Hide();
            choice.ShowDialog();
            this.Show();
        }

        private void optionsButtonClick(object sender, EventArgs e)
        {
            OptionsInterface options = new OptionsInterface();
            this.Hide();
            options.ShowDialog();
            this.Show();
        }

        private void exitButtonClick(object sender, EventArgs e)
        {
            Application.Exit();
        }        
    }
}
