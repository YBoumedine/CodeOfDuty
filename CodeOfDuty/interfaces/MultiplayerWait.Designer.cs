﻿namespace CodeOfDuty
{
    partial class MultiplayerWait
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchOppButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // searchOppButton
            // 
            this.searchOppButton.BackColor = System.Drawing.Color.Transparent;
            this.searchOppButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.searchOppButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchOppButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchOppButton.Location = new System.Drawing.Point(185, 298);
            this.searchOppButton.Name = "searchOppButton";
            this.searchOppButton.Size = new System.Drawing.Size(288, 99);
            this.searchOppButton.TabIndex = 0;
            this.searchOppButton.Text = "Search opponent";
            this.searchOppButton.UseVisualStyleBackColor = false;
            this.searchOppButton.Click += new System.EventHandler(this.searchOppButton_Click);
            // 
            // backButton
            // 
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(185, 460);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(288, 99);
            this.backButton.TabIndex = 1;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // MultiplayerWait
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.searchOppButton);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MultiplayerWait";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MultiplayerWait";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MultiplayerWait_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button searchOppButton;
        private System.Windows.Forms.Button backButton;
    }
}