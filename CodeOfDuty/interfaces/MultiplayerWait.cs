﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeOfDuty
{
    public partial class MultiplayerWait : Form
    {
        public MultiplayerWait()
        {
            InitializeComponent();
        }

        private void MultiplayerWait_Load(object sender, EventArgs e)
        {

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchOppButton_Click(object sender, EventArgs e)
        {
            searchOppButton.Enabled = false;
            if (!searchOppButton.Enabled)
            {
                searchOppButton.ForeColor = SystemColors.ControlDarkDark;
            }            
        }
    }
}
