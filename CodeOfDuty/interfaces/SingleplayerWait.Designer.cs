﻿namespace CodeOfDuty
{
    partial class SingleplayerWait
    {
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button chooseLevel;
        private System.Windows.Forms.ComboBox levelDB;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.chooseLevel = new System.Windows.Forms.Button();
            this.levelDB = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(185, 460);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(288, 99);
            this.backButton.TabIndex = 1;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // chooseLevel
            // 
            this.chooseLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chooseLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseLevel.Location = new System.Drawing.Point(185, 111);
            this.chooseLevel.Name = "chooseLevel";
            this.chooseLevel.Size = new System.Drawing.Size(288, 99);
            this.chooseLevel.TabIndex = 2;
            this.chooseLevel.Text = "Choose Level";
            this.chooseLevel.UseVisualStyleBackColor = true;
            this.chooseLevel.Click += new System.EventHandler(this.chooseLevel_Click);
            // 
            // levelDB
            // 
            this.levelDB.FormattingEnabled = true;
            this.levelDB.Location = new System.Drawing.Point(693, 111);
            this.levelDB.Name = "levelDB";
            this.levelDB.Size = new System.Drawing.Size(178, 21);
            this.levelDB.TabIndex = 3;
            // 
            // SingleplayerWait
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.levelDB);
            this.Controls.Add(this.chooseLevel);
            this.Controls.Add(this.backButton);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SingleplayerWait";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Singleplayer Wait";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SinglePlayer_Load);
            this.ResumeLayout(false);

        }

        #endregion

 

    }
}