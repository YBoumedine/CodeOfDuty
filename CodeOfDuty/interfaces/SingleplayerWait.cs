﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace CodeOfDuty
{
    public partial class SingleplayerWait : Form
    {
        
        public int index;

        public SingleplayerWait()
        {
            InitializeComponent();
            
            FillLevelDb();
        }

        void FillLevelDb()
        {
            int counter = 0;
            if(game.Game.questionsDictionary.Count == 0)
            {
                game.Game.CreateQuestionDictionnary();
                
            }

            Console.WriteLine(game.Game.questionsDictionary.Count);

            while (counter < game.Game.questionsDictionary.Count )
            {
                levelDB.Items.Add(game.Game.questionsDictionary.ElementAt(counter).Key);
                counter++;
            }
            
        }

        private void SinglePlayer_Load(object sender, EventArgs e)
        {

        }
        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chooseLevel_Click(object sender, EventArgs e)
        {
            
            int i = Convert.ToInt32(levelDB.SelectedItem.ToString());
            Question qTemp = game.Game.questionsDictionary.ElementAt(i-1).Value;            
            GameConsole gameConsole = new GameConsole();
            gameConsole.setQuestionDescription(qTemp.getQuestion());
            gameConsole.questionID = i;
            this.Hide();
            gameConsole.ShowDialog();
            this.Show();

        }
    }
}
