﻿using ScintillaNET;
using System;
using System.Drawing;

namespace CodeOfDuty
{
    partial class TutorialInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.tutorialVideoBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(34, 674);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(348, 71);
            this.backButton.TabIndex = 3;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // tutorialVideoBrowser
            // 
            this.tutorialVideoBrowser.Location = new System.Drawing.Point(34, 28);
            this.tutorialVideoBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.tutorialVideoBrowser.Name = "tutorialVideoBrowser";
            this.tutorialVideoBrowser.Size = new System.Drawing.Size(1305, 615);
            this.tutorialVideoBrowser.TabIndex = 5;
            this.tutorialVideoBrowser.Url = new System.Uri("https://www.youtube.com/watch?v=kaKsHfclxvQ", System.UriKind.Absolute);
            // 
            // TutorialInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.tutorialVideoBrowser);
            this.Controls.Add(this.backButton);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TutorialInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GameInterface";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.WebBrowser tutorialVideoBrowser;
    }
}